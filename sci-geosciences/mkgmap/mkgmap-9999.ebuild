# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit java-pkg-2 java-ant-2 subversion

ESVN_REPO_URI="http://svn.parabola.me.uk/mkgmap/trunk/"

DESCRIPTION="Tool to create garmin maps"
HOMEPAGE="http://wiki.openstreetmap.org/index.php/Mkgmap"
SRC_URI=""
LICENSE="GPL-2"
SLOT="0"

#No Keywords for overlay
KEYWORDS=""

IUSE=""
DEPEND=">=virtual/jdk-1.6"
RDEPEND=">=virtual/jre-1.6"
S="${WORKDIR}/${PN}"

src_compile() {
	JAVA_ANT_ENCODING=UTF-8
	eant dist
}

src_install() {
	java-pkg_newjar "dist/${PN}.jar" || die "java-pkg_newjar failed"
	java-pkg_dolauncher "${PN}" --jar "${PN}.jar" || die "java-pkg_dolauncher failed"

	dodoc dist/README dist/doc/Credits dist/doc/*.txt || die "dodoc failed"
	doman dist/doc/mkgmap.1 || die "doman failed"
}
