# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2
inherit eutils qt4 subversion

DESCRIPTION="Great Qt4 GUI front-end for mplayer"
HOMEPAGE="http://smplayer.sourceforge.net/"
ESVN_REPO_URI="http://smplayer.svn.sourceforge.net/svnroot/smplayer/smplayer/trunk"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE="debug"
DEPEND="x11-libs/qt-gui:4"
RDEPEND="${DEPEND}
	>=media-video/mplayer-1.0_rc2[ass,png]"

LANGS="bg ca cs de en_US es eu fi fr gl hu it ja ka ko ku mk nl pl pt_BR pt_PT sk
	sl sr sv tr zh_CN zh_TW"
NOLONGLANGS="ar_SY el_GR ro_RO ru_RU uk_UA"
for X in ${LANGS}; do
	IUSE="${IUSE} linguas_${X}"
done
for X in ${NOLONGLANGS}; do
	IUSE="${IUSE} linguas_${X%_*}"
done

src_prepare() {
	# For Version Branding
	cd "${ESVN_STORE_DIR}/${ESVN_CO_DIR}/${ESVN_PROJECT}/${ESVN_REPO_URI##*/}"
	./get_svn_revision.sh
	mv src/svn_revision.h "${S}"/src/
	mv svn_revision "${S}"/

	cd "${S}"

	# Fix paths in Makefile and allow parallel building
	sed -i -e "/^PREFIX=/s:/usr/local:/usr:" \
		-e "/^CONF_PREFIX=/s:\$(PREFIX)::" \
		-e "/^DOC_PATH=/s:packages/smplayer:${PF}:" \
		-e '/get_svn_revision.sh/,+2c\
	cd src && $(DEFS) $(MAKE)' \
		"${S}"/Makefile || die "sed failed"
	if ! use debug ; then
		sed -i "s:#DEFINES += NO_DEBUG_ON_CONSOLE:DEFINES += NO_DEBUG_ON_CONSOLE:" \
			"${S}"/src/smplayer.pro || die "sed failed"
	fi
}

src_configure() {
	eqmake4 src/${PN}.pro -o src/Makefile
}

src_compile() {
	emake || die "emake failed"

	# Generate translations
	cd "${S}"/src/translations
	local LANG=
	for LANG in ${LINGUAS}; do
		if has ${LANG} ${LANGS}; then
			einfo "Generating ${LANG} translation ..."
			lrelease ${PN}_${LANG}.ts || die "Failed to generate ${LANG} translation!"
			continue
		elif [[ " ${NOLONGLANGS} " == *" ${LANG}_"* ]]; then
			local X=
			for X in ${NOLONGLANGS}; do
				if [[ "${LANG}" == "${X%_*}" ]]; then
					einfo "Generating ${X} translation ..."
					lrelease ${PN}_${X}.ts || die "Failed to generate ${X} translation!"
					continue 2
				fi
			done
		fi
		ewarn "Sorry, but ${PN} does not support the ${LANG} LINGUA."
	done
	# install fails when no translation is present (bug 244370)
	[[ -f *.qm ]] || lrelease ${PN}_en_US.ts
}

src_install() {
	# remove unneeded copies of GPL
	rm Copying.txt docs/en/gpl.html docs/ru/gpl.html
	for i in de es ja nl ro ; do
		rm -rf docs/$i
	done

	# remove windows-only files
	rm *.bat

	emake DESTDIR="${D}" install || die "emake install failed"
	prepalldocs
}
